# Turtlebot miniproject

## Aalborg University. 
### Master in Autonomous Systems. Group 4
##### Jonas Vejlg�rd Kristensen
##### Louis Meyer
##### Miguel Descalzo Casado
##### Valeria Alexandra Feraru                         


### The project contains four important files.

### main.py
Here, all the workflow is contained. Executes the code and makes the necessary calls to other classes.


### armPlane.py
It is a class that simulates the 2D plane where the inverse kinematics are calculated in order to find out the joint angles of the robotic arm. It has as parameters the measures of the robotic arm, in order to calculate correctly the inverse kinematics


### location.py
It is a class that creates locations with multiple parameters as the location in different reference frames.


### roboticArm.py
It is class where the calls to the robotic arm are being done. It has as parameters the values of the five different joints.