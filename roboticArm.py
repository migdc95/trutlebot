#!/usr/bin/env python
import roslib

roslib.load_manifest('hello_ros')

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import shape_msgs.msg as shape_msgs

from std_msgs.msg import String


class RoboticArm():

    jointValueTarget0 = 0
    jointValueTarget1 = 0
    jointValueTarget2 = 0
    jointValueTarget3 = 0
    jointValueTarget4 = 0


    def __init__(self):
        pass

    def updateJoint(self, jointAngles):
        self.jointValueTarget0 = float(jointAngles[0])
        self.jointValueTarget1 = float(jointAngles[1])
        self.jointValueTarget2 = float(jointAngles[2])
        self.jointValueTarget3 = float(jointAngles[3])
        self.jointValueTarget4 = float(jointAngles[4])

    def move_group_python_interface_tutorial(self):
        # BEGIN_TUTORIAL
        # First initialize moveit_commander and rospy.
        print "============ Starting tutorial setup"
        moveit_commander.roscpp_initialize(sys.argv)
        scene = moveit_commander.PlanningSceneInterface()
        robot = moveit_commander.RobotCommander()

        ## Instantiate a MoveGroupCommander object.  This object is an interface
        ## to one group of joints.  In this case the group is the joints in the left
        ## arm.  This interface can be used to plan and execute motions on the left
        ## arm.
        group = moveit_commander.MoveGroupCommander("arm")

        ## We create this DisplayTrajectory publisher which is used below to publish
        ## trajectories for RVIZ to visualize.
        display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory)

        ## Wait for RVIZ to initialize. This sleep is ONLY to allow Rviz to come up.
        # print "============ Waiting for RVIZ..."
        # rospy.sleep(2)
        # print "============ Starting tutorial "

        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        ##
        ## We can get the name of the reference frame for this robot
        print "============ Reference frame: %s" % group.get_planning_frame()

        ## We can also print the name of the end-effector link for this group
        print "============ Reference frame: %s" % group.get_end_effector_link()

        ## We can get a list of all the groups in the robot
        print "============ Robot Groups:"
        print robot.get_group_names()

        ## Sometimes for debugging it is useful to print the entire state of the
        ## robot.
        print "============ Printing robot state"
        print robot.get_current_state()
        print "============"

        # rospy.sleep(2)

        print "============ Generating plan "

        group.set_joint_value_target([self.jointValueTarget0, self.jointValueTarget1, self.jointValueTarget2, self.jointValueTarget3, self.jointValueTarget4])

        ## Let's setup the planner
        group.set_planning_time(2.0)
        group.set_goal_orientation_tolerance(1)
        group.set_goal_tolerance(1)
        group.set_goal_joint_tolerance(0.1)
        group.set_num_planning_attempts(20)

        ## Now, we call the planner to compute the plan
        ## and visualize it if successful
        ## Note that we are just planning, not asking move_group
        ## to actually move the robot
        plan1 = group.plan()

        group.go(wait=True)

        ## When finished shut down moveit_commander.
        moveit_commander.roscpp_shutdown()

        ## END_TUTORIAL

        print "============ STOPPING"

 