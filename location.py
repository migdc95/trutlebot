import math

class Location:

    transformationMatrix = [0.4079, 3.232, 0.537] # IN METERS
    transformationAngle = 1.0505 # IN RADIANS

    viconX = 0.0
    viconY = 0.0
    viconZ = 0.0

    robotFrameX = 0.0
    robotFrameY = 0.0
    robotFrameZ = 0.0

    quaternions = (0, 0, 0, 1)

    pose = [(0, 0, 0), (0, 0, 0, 0)]

    def __init__(self, _x, _y, _z):
        self.viconX = _x
        self.viconY = _y
        self.viconZ = _z



    def updateViconCoordinates(self, x, y, z):
        self.viconX = x
        self.viconY = y
        self.viconZ = z


    def updateToRobotFrame(self):
        print "inside location file the vicon coordinates are", self.viconX, self.viconY
        cosAngle = math.cos(self.transformationAngle)
        sinAngle = math.sin(self.transformationAngle)

        print "transformation angle, cosine and sin", self.transformationAngle, cosAngle, sinAngle

        newX = cosAngle * self.viconX + sinAngle * self.viconY - cosAngle * self.transformationMatrix[0] - sinAngle * self.transformationMatrix[1]
        newY = - sinAngle * self.viconX + cosAngle * self.viconY - sinAngle * self.transformationMatrix[0] - cosAngle * self.transformationMatrix[1]

        self.robotFrameX = newX
        self.robotFrameY = newY
        self.robotFrameZ = self.viconZ - self.transformationMatrix[2]

    def updatePose(self):
        self.pose = [(self.robotFrameX, self.robotFrameY, self.robotFrameZ), self.quaternions]
