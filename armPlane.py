import math

class ArmPlane:

    xDistance = 0
    zDistance = 0

    arm1 = 0.15
    arm1_2 = 0.04
    arm2 = 0.15
    arm3 = 0.14

    alpha0 = 0
    alpha1 = 0
    alpha2 = 0
    alpha3 = 0
    alpha4 = 0

    alpha1Prime = 0
    alpha2Prime = 0
    alpha3Prime = 0

    solution = False
    minTolerance = 0.01

    jointAngles = [alpha0, alpha1, alpha2, alpha3, alpha4]

    def __init__(self, _x, _z, _alpha0):
        self.xDistance = _x
        self.zDistance = _z
        self.alpha0 = _alpha0


    def parallelApproach(self):
        for i in range(180, 0, -1):
            self.alpha1Prime = i
            if not self.solution:
                for j in range(180, 0, -1):
                    self.alpha2Prime = j

                    relativeX = self.arm1 * math.cos(math.radians(self.alpha1Prime)) + self.arm1_2 * math.sin(math.radians(self.alpha1Prime)) + self.arm2 * math.cos(math.radians(self.alpha2Prime))
                    relativeZ = self.arm1 * math.sin(math.radians(self.alpha1Prime)) - self.arm1_2 * math.cos(math.radians(self.alpha1Prime)) + self.arm2 * math.sin(math.radians(self.alpha2Prime))

                    if math.fabs(self.xDistance - self.arm3 - relativeX) <= self.minTolerance and math.fabs(self.zDistance - relativeZ) <= self.minTolerance:
                        self.alpha1 = 90 - self.alpha1Prime
                        self.alpha2 = self.alpha1Prime - self.alpha2Prime - 90
                        self.alpha3 = - self.alpha1 - self.alpha2

                        if self.alpha1 > 90 or self.alpha1 < -90 or self.alpha2 > 90 or self.alpha2 < -90 or self.alpha3 > 90 or self.alpha3 < -90:  # OUT OF OUR LIMITS
                            pass
                        else:
                            print "Can go parallel:", relativeX, relativeZ, self.alpha1Prime, self.alpha2Prime, self.alpha3Prime
                            self.solution = True

                            # CONVERT FROM DEGREES TO RADIANS
                            self.alpha1 = math.radians(self.alpha1)
                            self.alpha2 = math.radians(self.alpha2)
                            self.alpha3 = math.radians(self.alpha3)

                            self.updateJoints()
                            return True


    def notParallel(self):
        for i in range(180, 0, -1):
            self.alpha1Prime = i
            if not self.solution:
                for j in range(180, 0, -1):
                    self.alpha2Prime = j
                    if not self.solution:
                        for k in range(180, 0, -1):
                            self.alpha3Prime = k

                            relativeX = self.arm1 * math.cos(math.radians(self.alpha1Prime)) + self.arm1_2 * math.sin(math.radians(self.alpha1Prime)) + self.arm2 * math.cos(math.radians(self.alpha2Prime)) + self.arm3 * math.cos(math.radians(self.alpha3Prime))
                            relativeZ = self.arm1 * math.sin(math.radians(self.alpha1Prime)) - self.arm1_2 * math.cos(math.radians(self.alpha1Prime)) + self.arm2 * math.sin(math.radians(self.alpha2Prime)) + self.arm3 * math.sin(math.radians(self.alpha3Prime))

                            if math.fabs(self.xDistance - relativeX) <= self.minTolerance and math.fabs(self.zDistance - relativeZ) <= self.minTolerance:

                                self.alpha1 = 90 - self.alpha1Prime
                                self.alpha2 = self.alpha1Prime - self.alpha2Prime - 90
                                self.alpha3 = self.alpha2Prime - self.alpha3Prime

                                if self.alpha1 >= 90 or self.alpha1 <= -90 or self.alpha2 >= 90 or self.alpha2 <= -90 or self.alpha3 >= 90 or self.alpha3 <= -90:  # OUT OF OUR LIMITS
                                    pass
                                else:
                                    print "We can't reach it parallel:", relativeX, relativeZ, self.alpha1Prime, self.alpha2Prime, self.alpha3Prime
                                    self.solution = True

                                    # CONVERT FROM DEGREES TO RADIANS
                                    self.alpha1 = math.radians(self.alpha1)
                                    self.alpha2 = math.radians(self.alpha2)
                                    self.alpha3 = math.radians(self.alpha3)

                                    self.updateJoints()
                                    break

                            if i == 180 and j == 180 and k == 180:
                                self.minTolerance *= 2


    def updateJoints(self):
        self.jointAngles = [self.alpha0, self.alpha1, self.alpha2, self.alpha3, self.alpha4]


    def printProperties(self):
        print "From this position the joints have to be in the following angles:"
        print "Alpha 0:", self.alpha0
        print "Alpha 1:", self.alpha1
        print "Alpha 2:", self.alpha2
        print "Alpha 3:", self.alpha3
        print "Alpha 4:", self.alpha4