#!/usr/bin/env python
import rospy
import actionlib
import math
from geometry_msgs.msg import Twist, Point, TransformStamped
import tf
from location import Location
from armPlane import ArmPlane
from roboticArm import RoboticArm
import time

#from nav_msgs import Odometry

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

# ---------------------------------------------------------------
# --------------- GLOBAL VARIABLES DECLARATION ------------------
# ---------------------------------------------------------------
global transformationMatrix, angleTransformation, targetPosition, targetOrientation, goal_pose

transformationMatrix = [0.4079, 3.232, 0.537] # IN METERS
transformationAngle = 1.0505 # IN RADIANS
targetPosition = [0, 0]
targetOrientation = 0
approachPoint = [[(0, 0, 0), (0, 0, 0, 0)]]
distanceToGoal = 0.2

robotPositionX = 0  
robotPositionY = 0
robotPositionZ = 0

targetLocation = Location(0, 0, 0)
approachLocation = Location(0, 0, 0)
baseLocation = Location(0, 0, 0)

viconX = 0
viconY = 0
viconZ = 0

# ---------------------------------------------------------------
# ---------------------- VICON CALL BACK ------------------------
# ---------------------------------------------------------------

def flagCallback(data):

    rob = data  # callback function for ROS subscriber
    global viconX, viconY, viconZ
    viconX = (rob.transform.translation.x)
    viconY = (rob.transform.translation.y)
    viconZ = (rob.transform.translation.z)

    targetLocation.updateViconCoordinates(viconX, viconY, viconZ)

    #rot_rob = rob.transform.rotation
    #(r, p, rotationAroundXAxis) = tf.transformations.euler_from_quaternion([rot_rob.x, rot_rob.y, rot_rob.z, rot_rob.w])
    #robotRotation = rotationAroundXAxis - angleTransformation


def main():
    # create subscriber to the topic of our robot in Vicon
    kpos = rospy.Subscriber('/vicon/trialObject_grp5/trialObject_grp5', TransformStamped, flagCallback, queue_size=80)

    # CREATE AN INSTANCE OF THE ROBOTIC ARM SO THAT WE CAN CALL IT
    roboticArm = RoboticArm()
    print "created robotic arm"

    time.sleep(5)

    while not rospy.is_shutdown():

        #targetLocation.viconX = viconX
        #targetLocation.viconY = viconY
        #targetLocation.viconZ = viconZ

        # WE CONVERT THE TARGET LOCATION TO THE ROBOT'S FRAME OF REFERENCE
        targetLocation.updateToRobotFrame()
        print "target Location:", targetLocation.robotFrameX, targetLocation.robotFrameY

        # WE FIND A POINT CLOSE TO THE TARGET TO STOP
        (approachLocation.robotFrameX, approachLocation.robotFrameY) = defineApproachPoint(targetLocation)
        print "approach Location:", approachLocation.robotFrameX, approachLocation.robotFrameY

        # WE UPDATE THE POSE VARIABLE WITH THE NEW VALUES
        approachLocation.updatePose()
        print "updated pose of approachLocation"

        # ORDER THE ROBOT TO GO TO THE APPROACH LOCATION
        goToTargetPoint(approachLocation.pose)

        # CALCULATE THE ANGLE TO ROTATE ON THE BASE AND THE DISTANCE BETWEEN THE BASE OF THE ARM AND THE TARGET IN X AND Z COORDINATES
        (tempX, tempZ, tempAlpha0) = distanceFromRobotToFlag(approachLocation, targetLocation)
        print "angles and distances for arm calculated", tempX, tempZ, tempAlpha0

        # CREATE AN INSTANCE OF THE PLANE TO FIND OUT THE VALUES OF THE JOINTS
        armPlane = ArmPlane(tempX, tempZ, tempAlpha0)
        print "arm plane created"

        # CHECK IF WE CAN APPROACH IT WITH THE ENDEFECTOR PARALLEL TO GROUND
        if not armPlane.parallelApproach():
            # IF NOT POSSIBLE WE JUST FIND ANY CONFIGURATION
            armPlane.notParallel()

        print "joints calculated:", armPlane.jointAngles

        # WE PASS AS A PARAMETER THE JOINT ANGLES
        roboticArm.updateJoint(armPlane.jointAngles)
        print "passed joint values to arm"

        # WE ORDER THE ROBOT TO MOVE THE ARM WITH THE JOINT CONFIGURATION
        roboticArm.move_group_python_interface_tutorial()
        print "moving robotic arm towards objective"

        # AFTER GRABBING THE FLAG WE PLACE IT IN ALMOST THE REST POSITION
        safePositionPlane = ArmPlane(0.14, 0.04, 0)

        if not safePositionPlane.parallelApproach():
            safePositionPlane.notParallel()

        print "joints calculated for safe state:", safePositionPlane.jointAngles

        # WE PASS AS A PARAMETER THE JOINT ANGLES
        roboticArm.updateJoint(safePositionPlane.jointAngles)

        # WE ORDER THE ROBOT TO MOVE THE ARM WITH THE JOINT CONFIGURATION
        roboticArm.move_group_python_interface_tutorial()
        print "moving arm to safe position"

        # WE UPDATE THE POSE VARIABLE WITH THE NEW VALUES
        baseLocation.updatePose()
        print "uodating base location pose"

        # WE RETURN TO THE BASE
        goToTargetPoint(baseLocation.pose)
        print "going to base location"

        # END


# ---------------------------------------------------------------
# --------------- FIND POINT CLOSE TO TARGET --------------------
# ---------------------------------------------------------------
def defineApproachPoint(_location):
    if _location.robotFrameX >= 0:
        _targetX = _location.robotFrameX - distanceToGoal
    else:
        _targetX = _location.robotFrameX + distanceToGoal

    if _location.robotFrameY >= 0:
        _targetY = _location.robotFrameY - distanceToGoal
    else:
        _targetY = _location.robotFrameY + distanceToGoal

    return [_targetX, _targetY]


# ---------------------------------------------------------------
# ---FIND DISTANCES TO CREATE MAP FOR TJE INVERSE KINEMATICS ----
# ---------------------------------------------------------------
def distanceFromRobotToFlag(_initialPosition, _targetPosition):
    x1 = _initialPosition.robotFrameX
    y1 = _initialPosition.robotFrameY
    z1 = _initialPosition.robotFrameZ

    x2 = _targetPosition.robotFrameX
    y2 = _targetPosition.robotFrameY
    z2 = _targetPosition.robotFrameZ

    x = math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
    z = z2 - z1

    desiredRotation = math.atan2(x2 - x1, y2 - y1) # IT IS IN RADIANS

    return [desiredRotation, x, z]


# ---------------------------------------------------------------
# ------------- ORDER ROBOT TO GO TO SOME LOCATION --------------
# ---------------------------------------------------------------
def goToTargetPoint(posePoint):
    goal = goal_pose(posePoint)
    client.send_goal(goal)
    print ("Going towards goal")
    client.wait_for_result()
    print ("Goal reached")
    rospy.sleep(3)


def goal_pose(pose):
    goal_pose = MoveBaseGoal()
    goal_pose.target_pose.header.frame_id = 'map'
    goal_pose.target_pose.pose.position.x = pose[0][0]
    goal_pose.target_pose.pose.position.y = pose[0][1]
    goal_pose.target_pose.pose.position.z = pose[0][2]
    goal_pose.target_pose.pose.orientation.x = pose[1][0]
    goal_pose.target_pose.pose.orientation.y = pose[1][1]
    goal_pose.target_pose.pose.orientation.z = pose[1][2]
    goal_pose.target_pose.pose.orientation.w = pose[1][3]

    return goal_pose


if __name__ == '__main__':
    rospy.init_node('patrol')
    client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
    client.wait_for_server()

    main()
